import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const Search = () => import('./components/Search')
const MovieDetail = () => import('./components/MovieDetail')

const router = new Router({
    mode: 'history',
    scrollBehavior(to, from, savedPosition) {
        if(savedPosition) {
            return savedPosition
        } else if(to.hash) {
            return { selector: to.hash }
        } else {
            return { x: 0, y: 0 }
        }
    },
    routes: [{
        name: 'inicio',
        path: '/',
        // component: Inicio
        components: {
            default: Search,
        }
    }, {
        path: '/movie',
        // component: Usuario,
        components: {
            default: MovieDetail
        },
        props: true,
        children: [
            { path: '', component: Search },
            { path: ':id', component: MovieDetail, props: true,
                /*beforeEnter: (to, from, next) => {
                    console.log('antes da rota -> usuário detalhe')
                    next()
                }*/ },
        ]
    }, {
        path: '/redirecionar',
        redirect: '/'
    }, {
        path: '*',
        redirect: '/'
    }]
})

router.beforeEach((to, from, next) => {
    console.log('antes das rotas -> global')
    next()
})

export default router